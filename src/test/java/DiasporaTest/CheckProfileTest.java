package DiasporaTest;

import org.junit.BeforeClass;
import org.junit.Test;

import static DiasporaTest.Page.*;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by student on 06.06.15.
 */
public class CheckProfileTest {

    @BeforeClass
    public static void checkProfileTestInitialise (){
        Helpers.dropCreateSeedDiasporaDB();
        open("http://localhost:3000/");
    }

    @Test
    public void profileActions()
    {
        //Login and proceed to Profile
        login(login1);
        $(".user-menu-more-indicator").click();
        $$(".user-menu-item>a").findBy(text("Profile")).click();

        $("#name").shouldHave(text(login1));
        $$("#profile_information>li>h4").shouldHave(exactTexts("Bio", "Location", "Gender", "Birthday"));
        $$(".ltr>p").shouldHave(exactTexts(bio, location));


        //Proceed to Contacts
        $(".user-menu-more-indicator").click();
        $$(".user-menu-item>a").findBy(text("Contacts")).click();

        $(".span3>h3").shouldHave(text("Contacts"));
        $(".header>h3").shouldHave(text("My Contacts"));
        $$(".span3>div>ul>li").shouldHave(size(5));


        //Proceed to Settings
        $(".user-menu-more-indicator").click();
        $$(".user-menu-item>a").findBy(text("Settings")).click();

        $("#section_header>h2").shouldHave(text("Settings"));
        $$(".span6>h3").shouldHave(texts("Your diaspora* ID", "Your email", "Export data", "Close account"));


        //Proceed to Help
        $(".user-menu-more-indicator").click();
        $$(".user-menu-item>a").findBy(text("Help")).click();

        $(".help_header").shouldHave(text("Help"));
        $(".faq_getting_help>h1").shouldHave(text("diaspora* FAQ"));


        //Logout
        $(".user-menu-more-indicator").click();
        $$(".user-menu-item>a").findBy(text("Log out")).click();

        $$(".login").shouldHave(exactTexts("Sign up", "Sign in"));
        $("#user_username").setValue(login1);
        $("#user_password").setValue(password);
        $("[name='commit']").click();
        $(".stream_title").shouldHave(text("Stream"));











    }
}
