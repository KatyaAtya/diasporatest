package DiasporaTest;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static DiasporaTest.Page.login;
import static DiasporaTest.Page.login1;
import static DiasporaTest.PageProfileFunctions.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by student on 07.06.15.
 */
public class ProfileFunctionsTest {

    @BeforeClass
    public static void checkProfileTestInitialise (){
        Helpers.dropCreateSeedDiasporaDB();

        open("http://localhost:3000/");
        login(login1);
        //expandDropdownArrow.click();
        //profileMenuItems.findBy(text("Profile")).click();
    }

    @Before
    public void returnToProfile()
    {
        expandDropdownArrow.click();
        profileMenuItems.findBy(text("Profile")).click();
    }

    @Test
    public void profileAddTags() {

        addTags();

        checkTagsWereAdded();

        sendMessageWithTag();
    }

    @Test
    public void profileCreatePost()
    {
        inputPostfield.setValue("A new post!");
        previewButton.click();

        contentOfMessage.shouldHave(text("A new post!"));
        shareButton.click();
        contentOfMessage.shouldHave(text("A new post!"));
    }

    @Test
    public void contactsTest()
    {
        $("#contacts_link").click();

        inviteToDiaspora();

        stopSharingStart();

        proceedToContactProfile();

    }

    @Test
    public void editProfileFunction()
    {

        editProfile();
        checkOnPageThatProfileUpdated();

   }




}
