package DiasporaTest;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by student on 06.06.15.
 */
public class Page {
    public static String login1 ="alice";
    public static String login2 ="bob";
    public static String login3 ="eve";

    public static String password ="evankorth";
    public static String bio="I am a cat lover and I love to run";
    public static String location="Earth";
    public static String gender="robot";
    public static String birthday=" June 07 2015 ";


    public static void login (String login){
        $("#login-link").click();
        $("#user_username").setValue(login);
        $("#user_password").setValue(Page.password);
        $("[name='commit']").click();
        $(".stream_title").shouldHave(text("Stream"));
    }
}
