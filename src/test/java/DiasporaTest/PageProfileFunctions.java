package DiasporaTest;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static DiasporaTest.Page.login1;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by student on 07.06.15.
 */
public class PageProfileFunctions {
    public static SelenideElement expandDropdownArrow = $(".user-menu-more-indicator");
    public static ElementsCollection profileMenuItems =$$(".user-menu-item>a");
    public static SelenideElement tagsInput = $("#tags");
    public static SelenideElement addTagPopup =$("#as-result-item-0>em");
    public static SelenideElement message =$(".message");
    public static SelenideElement contentOfMessage =$(".markdown-content>p");
    public static SelenideElement shareButton = $("#submit");
    public static SelenideElement previewButton =$(".btn.btn-default.post_preview_button");
    public static SelenideElement inputPostfield = $("#status_message_fake_text");

    public static void addTags()
    {   $(".add_tags>a").click();
        tagsInput.setValue("#tag1");
        addTagPopup.click();
        tagsInput.setValue("#tag2");
        addTagPopup.click();
        $("#update_profile").click();
        message.shouldHave(text("Profile updated"));}

    public static void checkTagsWereAdded()
    {   $(".user-menu-more-indicator").click();
        profileMenuItems.findBy(text("Profile")).click();
        $$(".description>a").shouldHave(exactTexts("#tag1", "#tag2"));}

    public static void sendMessageWithTag()
    {
        $$(".tag").findBy(text("#tag1")).click();
        inputPostfield.setValue("some text");
        previewButton.click();
        contentOfMessage.shouldHave(text("some text"));
        shareButton.click();
        contentOfMessage.shouldHave(text("some text"));
    }

    public static void inviteToDiaspora()
    {

        $(".btn.btn-link").click();
        $("#email_inviter_emails").setValue("test@test.com");
        $(".btn.btn-primary.creation").click();
        message.shouldHave(text("Invitations have been sent to: test@test.com"));
    }

    public static void stopSharingStart()
    {

        //Contact is present
        $(".header>h3").shouldHave(text("My contacts"));
        $(".info.diaspora_handle").shouldHave(text("bob@localhost:3000"));

        //Stop sharing
        $(".btn.btn-small.dropdown-toggle.green").click();
        $(".aspect_selector.selected>a").click();
        message.shouldHave(text("You have stopped sharing with Bob"));

        //Start sharing again
        $(".row-fluid").click();
        $(".btn.btn-small.dropdown-toggle.btn-default").click();
        $(".aspect_selector>a").click();
        message.shouldHave(text("You have started sharing with Bob!"));
    }

    public static void proceedToContactProfile()
    {
        $(".media-body>a").click();
        $("#name").shouldHave(text("Bob Grimm"));
    }

    public static void editProfile(){
        $("#edit_profile").click();
        $("#profile_first_name").setValue(login1 + "1");
        $("#profile_last_name").setValue("lastname");
        $("#profile_bio").setValue("I like music, photos and snowboards");
        $("#profile_location").setValue("Kyiv");
        $("#profile_gender").setValue("Female");
        $("#profile_date_year").selectOptionByValue("1991");
        $("#profile_date_month").selectOptionByValue("1");
        $("#profile_date_day").selectOptionByValue("6");


        $("#update_profile").click();
    }

    public static void checkOnPageThatProfileUpdated ()
    {
        message.shouldHave(text("Profile updated"));

        expandDropdownArrow.click();
        profileMenuItems.findBy(text("Profile")).click();

        $("#name").shouldHave(text(login1 + "1" + " " + "lastname"));
        $$(".ltr>p").shouldHave(exactTexts("I like music, photos and snowboards", "Kyiv"));
        $$("#profile_information>li").get(2).shouldHave(text("Female"));
        $$("#profile_information>li").get(3).shouldHave(text("January 06 1991"));
    }
}
